const GameState = Object.freeze({
    WELCOMING:  Symbol("welcoming"), 
    PLAYING:    Symbol("playing")
   });

export default class Game{
    constructor(){
        this.stateCur = GameState.WELCOMING;
    }
    
    makeAMove(sInput)
    {
        let sReply = "";
        switch(this.stateCur){
            
            case GameState.WELCOMING:
                this.nComputer = Math.ceil(Math.random() * 99);
                sReply = "I am thinking of a number between 1 and 10 ....please guess ";           
                this.stateCur = GameState.PLAYING;
                break;
            
            case GameState.PLAYING:
                if(sInput == this.nComputer){
                    sReply = "You are right ... I just thought of another number. Please guess it.";
                    this.nComputer = Math.ceil(Math.random() * 99);
                }else if(sInput - this.nComputer < 5){
                    sReply = "Too close ... try down!";
                }
                else if(sInput - this.nComputer < 15){
                    sReply = "Little bit far ... try down!";
                }
                else if(sInput - this.nComputer < 30){
                    sReply = "Little bit far ... try down!";
                }
                else if(sInput - this.nComputer < 40){
                    sReply = "Too far ... DOWN please!";
                }
                else if(this.nComputer - sInput < 5){
                    sReply = "Too close ... try up!";
                }
                else if(this.nComputer - sInput < 15){
                    sReply = "Little bit far ... try up!";
                }
                else if(this.nComputer - sInput < 30){
                    sReply = "Little bit far ... try up!";
                }
                else if(this.nComputer - sInput < 40){
                    sReply = "Too far ... DOWN up!";
                }
                else if(this.nComputer - sInput > 98 || sInput - this.nComputer < 98){
                    sReply = "Please, enter a number between 1 and 99!";
                }else {
                    sReply = "Enter a valid number!";
                }               
                break;  
        }
        return([sReply]);
    }
}